<?php
    /**
     * @author Clark Tomlinson
     * @since 9/27/12, 11:23 AM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2012
     *
     */

    require_once('../classes/K2_Api.php');

    class K2Test extends PHPUnit_Framework_TestCase
        {


        /**
         * @expectedException BadMethodCallException
         */
            public function testConstructorReturnsExceptionIfParametersAreMissing()
                {
                    $k2 = new K2API('foo');
                }

            public function testValidateDatabaseEntriesAreSet()
                {
                    $k2 = new K2API('insp', 'db');
                    $this->assertClassHasAttribute('dbname', 'K2API');
                }

            public function testMakeSureGeoLocationReturnsValidResults()
                {
                    $k2 = new K2API('insp', 'db');
                    $this->assertArrayHasKey('places', $k2->geoLocate('28277'));
                }

            public function testFormSubmissionDoesntThrowErrors()
                {
                    $k2 = new K2API('insp', 'db');

                    $data = array(
                        'FIRSTNAME'    => 'Clark',
                        'LASTNAME'     => 'Tomlinson',
                        'EMAILADDRESS' => 'fallen013@gmail.com',
                        'CRISIS'       => 0,
                        'ZIPCODE'      => 28277,
                        'CAMPAIGN'     => 'Comcast Thanks',
                        'PROVIDER'     => 'Time warner cable',
                        'OPTIN'        => 0,
                        'K2USERID'     => 1975468
                    );

                    $this->assertTrue($k2->handleSubmission($data));
                }
        }