<?php
    /**
     * @author Clark Tomlinson
     * @since 9/26/12, 4:09 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2012
     *
     */
    namespace K2;

    class API
        {
            public $debug;
            public $ajax;
            public $geolocating;

            protected $k2RetreiveUrl;
            protected $k2PostUrl;
            protected $client;
            protected $k2UserId;
        /**
         * Defines how submissions should be stored
         *
         * @example db = Database Only
         * @example k2 = Submit to k2 Only
         * @example both = Submit to k2 and store in local db
         * @var string
         */
            protected $storageType;

        /**
         * Holds Database Info for submission if selected storage type is db or both
         *
         * @var $db
         * @return \PDO
         */
            protected $db;

            private $callType;
            private $request;
            private $response;
            private $requiredFields = array(
                'FIRSTNAME',
                'LASTNAME',
                'PROVIDER',
                'CRISIS',
                'EMAILADDRESS',
                'ZIPCODE',
                'K2USERID',
                'OPTIN'
            );

            private $site;
            private $sites = array();


        /**
         * @param        $site
         * @param string $storageType db, k2, both
         * @param array  $config
         */
            public function __construct($site, $storageType = 'both', array $config = array())
                {
                    $this->client = trim(strtoupper($site));
                    $this->storageType = trim(strtolower($storageType));

                    if (!empty($config)) {
                        $this->init($config);
                    }
                }

        /**
         * Sets the configuration for the api
         * <code>
         *             private $sites = array(
        'INSP'    => array(
        'DB' => array(
        'host'  => 'localhost',
        'user'  => '',
        'pass'  => '',
        'name'  => '',
        'table' => ''
        ),
        'K2' => array(
        'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
        'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
        )
        ),
        'HALOGEN' => array(
        'DB' => array(
        'host'  => 'localhost',
        'user'  => '',
        'pass'  => '',
        'name'  => '',
        'table' => ''
        ),
        'K2' => array(
        'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
        'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
        )
        ),
        'DEBUG'   => array(
        'DB' => array(
        'host'  => 'localhost',
        'user'  => 'root',
        'pass'  => 'root',
        'name'  => 'k2apisubmission',
        'table' => 'api_requests'
        ),
        'K2' => array(
        'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
        'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
        )
        ),
        );
         * </code>
         *
         * @param array $config
         */
            public function init(array $config)
                {
                    $this->sites = $config;
                }

        /**
         * Sets needed k2 post and retrieval urls
         *
         * @param $site
         *
         * @throws \BadMethodCallException
         * @return bool
         */
            public function getSiteData($site)
                {
                    /*
                     * This method will store post/retrieve urls for our various sites
                     * and set them to class properties for use later
                    */

                    // Lets make this bad boy Uppercase to match the array keys
                    $site = strtoupper($site);
                    if ($this->debug || isset($_SERVER['WP_ENV']) && $_SERVER['WP_ENV'] == 'local') {
                        $this->site = $this->sites['DEBUG'];
                        return true;
                    } else {
                        if (array_key_exists($site, $this->sites)) {
                            $this->site = $this->sites[$site];
                            return true;
                        }
                        return false;
                    }
                }

        /**
         * returns necessary call type for the request
         *
         * @return mixed
         */
            protected final function callType()
                {
                    $this->getSiteData($this->client);

                    if ($this->callType == 'request') {
                        return $this->site['K2']['request'];
                    }
                    return $this->site['K2']['post'];
                }

        /**
         * Returns all providers in specified zip code
         *
         * @param $zipcode
         *
         * @return API
         */
            public function getProviders($zipcode)
                {
                    $this->callType = 'request';
                    $request = array(
                        'CLIENT'  => $this->client,
                        'ZIPCODE' => $zipcode
                    );

                    $this->request = $request;

                    return $this;
                }

        /**
         * @param $data
         *
         * @return mixed
         */
            public function handleSubmission($data)
                {
                    $this->callType('post');

                    if (!empty($errors)) {
                        return $this->displayErrors($errors);
                    }
                    // Check if geo locate field is provided and true
                    if (isset($_POST['geolocate']) && $_POST['geolocate'] === 'true' || $this->geolocating) {
                        $geo = $this->geoLocate($data['zipcode']);

                        $data['city'] = $geo['places'][0]['place name'];
                        $data['state'] = $geo['places'][0]['state'];
                    }

                    // Determine how data should be stored and proceed
                    switch ($this->storageType) {
                        case 'db':
                            if ($this->insertDb($data)) {
                                return $this->sendResponse();
                            }
                            break;
                        case 'k2':
                            return $this->sendRequest($data);
                            break;
                        case 'both':
                        default:
                            $this->insertDb($data);
                            return $this->sendRequest($data);
                            break;
                    }


                }

        /**
         * Inserts provided k2 submission data to provided database
         *
         * @param $data
         *
         * @throws \PDOException
         * @return string
         */
            private function insertDb($data)
                {

                    //Check if city and state survived the strtolower as it strips null values if they don't set them to null for insertion
                    if (!isset($data['city']) || !isset($data['state'])) {
                        $data['city'] = null;
                        $data['state'] = null;
                    }
                    if (!isset($data['campaign'])) {
                        $data['campaign'] = $data['source'];
                        unset($data['source']);
                    }

                    if (!isset($data['extra'])) {
                        $data['extra'] = '';
                    } else {
                        $data['extra'] = serialize($data['extra']);
                    }

                    $this->db = new \PDO("mysql:host={$this->site['DB']['host']};dbname={$this->site['DB']['name']}", $this->site['DB']['user'], $this->site['DB']['pass']);
                    $insert = $this->db->prepare("INSERT INTO `{$this->site['DB']['table']}` (`firstname`, `lastname`, `emailaddress`, `crisis`, `city`, `state`, `zipcode`, `campaign`, `provider`, `optin`, `k2userid`, `extra`) VALUES (:firstname, :lastname, :emailaddress, :crisis, :city, :state, :zipcode, :campaign, :provider, :optin, :k2userid, :extra)");

                    try {
                        $insert->execute($data);
                        $this->response = array('status' => 'success');
                        return true;
                    } catch (\PDOException $e) {
                        throw new \PDOException($e->getTraceAsString());
                    }
                }

        /**
         * Sends a request to k2 and returns the result
         *
         * @param $data
         *
         * @return mixed
         */
            public function sendRequest($data = null)
                {
                    if (is_null($data)) {
                        $data = $this->request;
                    }
                    // Add, remove, and rename some required k2 fields
                    $data['client'] = $this->client;
                    if (!isset($data['emailaddress'])) {
                        $data['emailaddress'] = '';
                    }

                    unset($data['city'], $data['state']);

                    $data = $this->toXml($data);

                    $ch = curl_init($this->callType());
                    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $reply = curl_exec($ch);
                    $info = curl_getinfo($ch);
                    curl_close($ch);

                    return $this->parseResponse($reply);
                }

        /**
         * Quick and easy parser to convert xml response to a usable array
         *
         * @param $response
         *
         * @return mixed
         */
            public function parseResponse($response)
                {
                    // Set k2userid in case we are posting data to k2 after zip retrieval
                    if (isset($this->response['K2USERID'])) {
                        $this->k2UserId = $this->response['K2USERID'];
                    }

                    // Store the response for later use if needed
                    $this->response = json_decode(json_encode((array) simplexml_load_string($response)), 1);

                    if (isset($this->response['PROVIDER'])) {
                        // Loop through all providers and url decode them
                        for ($i = 0; $i < count($this->response['PROVIDER']); $i++) {
                            $this->response['PROVIDER'][$i]['NAME'] = urldecode($this->response['PROVIDER'][$i]['NAME']);
                        }
                    }

                    return $this->sendResponse();

                }

        /**
         * @return string|void
         */
            public function sendResponse()
                {
                    if ($this->ajax) {
                        return json_encode($this->response);
                    }
                    return $this->response;
                }

        /**
         * returns array of errors encountered in processing
         *
         * @param $errors
         *
         * @return bool
         */
            public function displayErrors($errors)
                {
                    echo '<pre>';
                    print_r($errors);
                    echo '</pre>';
                    return false;
                }

        /**
         * Returns array containing location name state, and zip codes
         *
         * @param $zipcode
         *
         * @return mixed
         */
            public function geoLocate($zipcode)
                {
                    //get city and state from api via curl as ajax is not stable enough for current needs
                    $ch = curl_init('http://api.zippopotam.us/us/' . $zipcode);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $reply = curl_exec($ch);
                    curl_close($ch);

                    $reply = json_decode($reply, true);

                    return $reply;
                }

        /**
         * Converts data array into a valid XML request
         *
         * @param        $data
         * @param string $rootNodeName
         * @param null   $xml
         *
         * @return mixed
         */
            public function toXml($data, $rootNodeName = 'K2DATACOLLECTOR', $xml = null)
                {
                    // turn off compatibility mode as simple xml gets its panties in a wad if you don't.
                    if (ini_get('zend.ze1_compatibility_mode') == 1) {
                        ini_set('zend.ze1_compatibility_mode', 0);
                    }

                    if ($xml == null) {
                        $xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><$rootNodeName />");
                    }

                    // loop through the data passed in.
                    foreach ($data as $key => $value) {
                        // no numeric keys in our xml please!
                        if (is_numeric($key)) {
                            // make string key...
                            $key = "unknownNode_" . (string) $key;
                        }

                        $key = strtoupper($key);

                        // replace anything not alpha numeric
//                        $key = preg_replace('/[^a-z]/i', '', $key);

                        // if there is another array found recursively call this function
                        if (is_array($value)) {
                            $node = $xml->addChild($key);
                            // reclusive call.
                            $this->toXml($value, $rootNodeName, $node);
                        } else {
                            // add single node.
                            $value = htmlentities($value);
                            $xml->addChild($key, $value);
                        }

                    }
                    // pass back as string
                    return $xml->asXML();
                }
        }
