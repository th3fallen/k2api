<?php
    /**
     * @author Clark Tomlinson  <fallen013@gmail.com>
     * @since 11/26/12, 1:36 PM
     * @link http://www.clarkt.com
     * @copyright Clark Tomlinson © 2012
     *
     */
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);

    require_once (dirname(dirname(__FILE__)) . '/k2api.php');

    $config = array(
        'INSP'    => array(
            'DB' => array(
                'host'  => 'localhost',
                'user'  => '',
                'pass'  => '',
                'name'  => '',
                'table' => ''
            ),
            'K2' => array(
                'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
                'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
            )
        ),
        'HALOGEN' => array(
            'DB' => array(
                'host'  => 'localhost',
                'user'  => '',
                'pass'  => '',
                'name'  => '',
                'table' => ''
            ),
            'K2' => array(
                'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
                'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
            )
        ),
        'DEBUG'   => array(
            'DB' => array(
                'host'  => 'localhost',
                'user'  => 'root',
                'pass'  => 'root',
                'name'  => 'k2apisubmission',
                'table' => 'api_requests'
            ),
            'K2' => array(
                'post'    => 'http://{CLIENT}.viewerlink.tv/userXML.asp',
                'request' => 'http://{CLIENT}.viewerlink.tv/collectXML.asp'
            )
        ),
    );

    $k2 = new \K2\API('INSP', 'both', $config);
    $k2->ajax = true;
    
    if (isset($_SERVER['WP_ENV']) && $_SERVER['WP_ENV'] == 'local') {
        $k2->debug = true;
    }
    $k2->geolocating = true;

    switch ($_POST['action']) {
        case 'getProviders':
            echo $k2
                ->getProviders($_POST['zipcode'])
                ->sendRequest();
            break;

        case 'handleSubmissionZip':
            return true;
            break;

        case 'requestProvider':
            $data = array(
                'firstname'    => filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),
                'lastname'     => filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),
                'emailaddress' => filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                'crisis'       => filter_input(INPUT_POST, 'crisis', FILTER_SANITIZE_NUMBER_INT),
                'zipcode'      => filter_input(INPUT_POST, 'zipcode', FILTER_SANITIZE_NUMBER_INT),
                'source'       => 'findinsp',
                'provider'     => filter_input(INPUT_POST, 'provider', FILTER_SANITIZE_STRING),
                'optin'        => '0',
                'k2userid'     => filter_input(INPUT_POST, 'k2userid', FILTER_SANITIZE_NUMBER_INT),

            );
            echo $k2->handleSubmission($data);

            if ($_POST['newsletter'] == 'on') {
                require_once ('../../lyris/lyrisApi.php');

                $lyris = new \Lyris\API('2010002711', 'GMvxPpes9Xerg2P');

                $demographics = array(
                    '1' => filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),
                    '2' => filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),
                    '7' => filter_input(INPUT_POST, 'zipcode', FILTER_SANITIZE_NUMBER_INT)
                );

                $result = $lyris->memberAdd('192578',
                    filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                    $demographics);
            }
            break;

        case 'thankProvider':
            if ($_POST['newsletter'] == 'on') {
                require_once ('../../lyris/lyrisApi.php');

                $lyris = new \Lyris\API('2010002711', 'GMvxPpes9Xerg2P');

                $demographics = array(
                    '1' => filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),
                    '2' => filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),
                    '7' => filter_input(INPUT_POST, 'zipcode', FILTER_SANITIZE_NUMBER_INT)
                );

                $result = $lyris->memberAdd('192578',
                    filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                    $demographics);
            }
            echo json_encode(array(
                                  'status' => '1'
                             ));
            break;

        case 'subscribeNewsletter':
            require_once ('../../lyris/lyrisApi.php');

            $lyris = new \Lyris\API('2010002711', 'GMvxPpes9Xerg2P');

            $demographics = array(
                '1' => filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING),
                '2' => filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING),
                '7' => filter_input(INPUT_POST, 'zipcode', FILTER_SANITIZE_NUMBER_INT)
            );

            $result = $lyris->memberAdd('192578',
                filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL),
                $demographics);

            echo json_encode($result);
            break;

        case 'referFriend':
            require_once ('../../google/recaptcha/recaptchalib.php');
            require_once ('../../../vendor/swiftmailer/swift_required.php');


            // Get a key from https://www.google.com/recaptcha/admin/create
            $publickey = "6Lfa2soSAAAAABMftyR8J4ALIgRHdu3WKngA-kpI";
            $privatekey = "6Lfa2soSAAAAAP0bqvFxiTgrrcPPV31jdMhq0T_U";

            # the response from reCAPTCHA
            $resp = null;
            # the error code from reCAPTCHA, if any
            $error = null;

            # was there a reCAPTCHA response?
            if ($_POST["recaptcha_response_field"]) {
                $resp = recaptcha_check_answer($privatekey,
                    $_SERVER["REMOTE_ADDR"],
                    $_POST["recaptcha_challenge_field"],
                    $_POST["recaptcha_response_field"]);

                if ($resp->is_valid) {

                    $sendername = filter_input(INPUT_POST, 'sendername', FILTER_SANITIZE_STRING);
                    $senderemail = filter_input(INPUT_POST, 'senderemail', FILTER_SANITIZE_EMAIL);

                    $to = array();
                    for ($i = 0; $i <= 2; $i++) {
                        if (!empty($_POST['friendname'][$i])) {
                            $to[filter_input(INPUT_POST,
                                "friendemail[$i]",
                                FILTER_SANITIZE_EMAIL)] = filter_input(INPUT_POST,
                                "friendname[$i]",
                                FILTER_SANITIZE_STRING);
                        }
                    }

                    // Create the transport to send the messages
                    $transport = Swift_MailTransport::newInstance(false);

                    // Create the Mailer using the Transport
                    $mailer = Swift_Mailer::newInstance($transport);

                    // Create Your Message
                    $message = Swift_Message::newInstance("{$sendername} ({$senderemail}) recommends INSP Family Friendly Programming")
                        ->setFrom(array('do-not-reply@insp.com' => 'INSP'))
                        ->setBody("Dear ");

                    // Send your message
                    $failedRecipients = array();
                    $numSent = 0;

                    foreach ($to as $address => $name) {
                        if (is_int($address)) {
                            $message->setTo($name);
                        } else {
                            $message->setTo(array($address => $name));
                            $message->setBody("Dear {$name}, \n <br/> {$sendername} thought you might be intrested in <a href=\"http://www.insp.com\">INSP</a>",
                                'text/html');
                        }

                        $numSent += $mailer->send($message, $failedRecipients);
                    }

                    echo json_encode(array(
                                          'status' => 'success',
                                          'sent'   => $numSent
                                     ));
                } else {
                    # set the error code so that we can display it
                    $error = $resp->error;
                    echo json_encode(array(
                                          'status'  => 'invalid-captcha',
                                          'message' => $error,
                                     ));
                }
            }
            break;

        default:
            echo 'Action not found';
            break;
    }